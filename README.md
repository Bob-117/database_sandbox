# DATABASE SANDBOX

```sh
git clone
```

## Eucaryotes

Projet SQL d'une base de données de gestion de plantes en accord avec des standards de taxonomie.


https://fr.wikipedia.org/wiki/Taxonomie :

La taxonomie ou taxinomie est une branche des sciences naturelles qui a pour objet l'étude de la diversité du monde vivant. Cette activité consiste à décrire et circonscrire en termes d'espèces les organismes vivants et à les organiser en catégories hiérarchisées appelées taxons. 


Le point d'entrée de ce projet est une table `taxonomie` présentant un champ `parent` comme référence vers un autre taxon (foreign key sur la meme table).
L'objectif principal est d'utiliser et d'optimiser le langage SQL (sous MySQL) à travers des vues et des procédures stockées.

```sh
Eucaryotes/
            ...eucaryotes.sql
            ...populate.sql
            ...view.sql
            ...stored_process.sql
```
Utilisation :

Le script `eucaryotes.sql`instancie la base de donnée ainsi que les différentes tables.

On utilise `populate.sql` pour avoir quelques données.

Les scripts `view.sql` et `stored_process.sql` chargent le reste du projet.

<i>Todo : `assert.sql` </i>

 - [ ] Consulter tous les taxons référencés (`view.sql`)
 - [ ] Renseigner un nouveau taxon (depuis une liste de parents finie)
 - [ ] Consulter un taxon et son parent (`stored_process.sql`)
 - [ ] Consulter uniquement les cultivars (trigger, si plus de X enfants alors "arbre" devient une "famille" et plus seulement un taxon ordinaire => plus tard)
 - [ ] Consulter la classification complete d'un taxon
 - [ ] Consulter l'ensemble des taxons à N degrés
 - [ ] Ajouter les plantations d'un cultivar, les jardins, les utilisateurs
 - [ ] Ajouter les animaux (parasites)
 - [ ] AJouter tables pivot (plante x deteste parasite y mais plante z detruit parasite y  cf absinthe pucerons rosiers => plus tard)
 - [ ] Pour une zone donnée dans un jardin, quelles plantes ne surtout pas planter 
 - [ ] Pour une zone donnée dans un jardin, quelles plantes à planter 


## Base de données

### Mysql

Lancer un docker compose avec mysql et phpmyadmin

```sh
cd mysql
docker compose up
# http://localhost:8117 => root password
```

### PostgreSQL

Lancer un docker postgresql

```shell
docker pull postgres:latest
docker run --name=postgre_db -v ~/shared_with_postgres:/var/data -e POSTGRES_PASSWORD=1234 -p 5432:5432 postgres:latest

psql -h 0.0.0.0 -p 5432 -U postgres --password
psql -h 0.0.0.0 -U postgres -a -f directory/data.sql
```
