DROP DATABASE IF EXISTS `test_db`;
CREATE DATABASE IF NOT EXISTS `test_db` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS `test_db`.`test_table`(
	
    	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    	item VARCHAR(50) NOT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `test_db`.`test_table` (item) VALUES ('gibson'), ('python'), ('sql');