/* ****************
* DATABASE & USER *
*******************/
DROP DATABASE `Eucaryotes`;
CREATE DATABASE IF NOT EXISTS `Eucaryotes` DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*
CREATE USER IF NOT EXISTS `erwann`@`localhost` IDENTIFIED BY 'python';

GRANT ALL ON `Eucaryotes`.* TO 'erwann'@'localhost';

REVOKE INSERT ON `Eucaryotes`.* FROM 'erwann'@'localhost';
*/


/* ****************
* TABLES          *
*******************/
CREATE TABLE IF NOT EXISTS `Eucaryotes`.`taxonomie`(
	
    	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    	taxon VARCHAR(50) NOT NULL,
    	parent_id INT DEFAULT NULL,

    	FOREIGN KEY (parent_id) REFERENCES taxonomie(id) ON DELETE SET NULL ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE IF NOT EXISTS `Eucaryotes`.`informations_taxon`(
	
    	id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    	info TEXT DEFAULT NULL,
    	taxon_id INT NOT NULL,

    	FOREIGN KEY (taxon_id) REFERENCES taxonomie(id) ON DELETE SET NULL ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/* ****************
* POPULATE        *
*******************/
INSERT INTO `Eucaryotes`.`taxonomie` (id, taxon, parent_id) VALUES (-1, 'Eucaryotes', Null);
INSERT INTO `Eucaryotes`.`taxonomie` (id, taxon, parent_id) VALUES (1, 'Plantae', -1), (2, 'Fungi', -1), (3, 'Animalia', -1); 
INSERT INTO `Eucaryotes`.`taxonomie` (id, taxon, parent_id) VALUES
/* TOMATE */
(100, 'Tracheobionta', 1), -- Sous regne
(101, 'Magnoliophyta', 100), -- Division
(102, 'Magnoliopsida', 101), -- Classe
(103, 'Asteridae', 102), -- Sous classe
(104, 'Solanales', 103), -- Ordre
(105, 'Solanaceae', 104), -- Famille
(106, 'Solanum', 105), -- Genre
(107, 'Solanum lycopersicum', 106), -- Espece
/* LISERON */
(110, 'Convolvulaceae', 104), -- Famille
(111, 'Convolvulus', 110), -- Genre
(112, 'Convolvulus lanuginosus', 111), -- Espece liseron laineux protégé en PACA
(113, 'Convolvulus tricolor', 111), -- Espece liseron belle de jour
/* PATATE DOUCE */
(120, 'Ipomoeeae', 110), -- Tribu
(121, 'Ipomoea', 120), -- Genre
(122, 'Ipomoea batatas', 121), -- Espece
/* BLE */
(130, 'Liliopsida', 101), -- Classe
(131, 'Commelinidae', 130), -- Sous classe
(132, 'Cyperales', 131), -- Ordre
(133, 'Poaceae', 132), -- Famille
(134, 'Pooideae', 133), -- Sous famille
(135, 'Triticeae', 134), -- Tribu
(136, 'Triticum', 135), -- Genre
(137, 'Triticum turgidum', 136), -- Espece BLE DUR
(138, 'Triticum turgidum subsp. durum', 137), -- Sous espece
(139, 'Triticum aestivum', 136); -- Espece BLE TENDRE/FROMENT 	

/* INFORMATIONS TAXON */
INSERT INTO `Eucaryotes`.`informations_taxon` (info, taxon_id) VALUES 
('Espece liseron laineux protégé en PACA', 112), 
('Espece liseron belle de jour', 113)
('Du blé dur', 137),
('Du blé tendre / froment', 139),
('La patate douce', 122);



/* ****************
* STORED PROCESS  *
*******************/
DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.recuperer_id_parent_avec_id(IN taxon_id INT)
	BEGIN
		SELECT t.parent_id FROM `Eucaryotes`.`taxonomie` t WHERE t.id = taxon_id;
	END |
DELIMITER ;
-- CALL get_parent(2); 

DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.recuperer tout()
	BEGIN
		SELECT t.*, i.* 
		FROM `Eucaryotes`.`taxonomie` t 
		JOIN `Eucaryotes`.`informations_taxon` i
		WHERE t.id = i.taxon_id;
	END |
DELIMITER ;


DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.recuperer_la_branche_avec_nom(IN taxon VARCHAR(55), OUT nom_complet VARCHAR(1000))
	BEGIN
		DECLARE parent1;
		SET parent1 = SELECT t.id FROM `Eucaryotes`.`taxonomie` t WHERE t.taxon = taxon;
		SELECT t.parent_id FROM `Eucaryotes`.`taxonomie` t WHERE t.id = parent1;
	END |
DELIMITER ;

DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.`recuperer_la_branche_avec_nom`(IN taxon VARCHAR(55), OUT nom_complet VARCHAR(1000))
	BEGIN
    DECLARE parent1 VARCHAR(55)|
	SET parent1 = SELECT t.id FROM `Eucaryotes`.`taxonomie` t WHERE t.taxon = taxon
	SELECT t.parent_id FROM `Eucaryotes`.`taxonomie` t WHERE t.id = 2
	END |
DELIMITER ;

/* ****************
* TODO            *
*******************/
-- J'arrive et je consulte TOUT avec les infos associées
-- J'ai accès a un taxon et son arboresence
-- Je plante 1 cultivar particulier X dans une zone d'un jardin
-- J'ai accès aux autres cultivar parents de X avec de degré n 

/* ****************
* DATA & DROPBOX  *
*******************/ 
/* BLE 
Triticum aestivum (blé tendre)
Triticum compactum (blé hérisson)
Triticum dicoccoides (amidonnier sauvage)
Triticum dicoccon (amidonnier)
Triticum durum (blé dur)
Triticum monococcum (engrain ou petit épeautre)
Triticum sinskajae (engrain nu)
Triticum spelta (grand épeautre)
Triticum sphaerococcum (blé indien)
Triticum turanicum (blé de Khorasan ou Kamut)
Triticum turgidum (blé poulard)

Triticum aethiopicum
Triticum araraticum
Triticum boeoticum
Triticum carthlicum
Triticum macha
Triticum militinae
Triticum polonicum
Triticum timopheevii
Triticum ispahanicum
Triticum karamyschevii
*/

/*
tuhya

Règne 	Plantae
Division 	Pinophyta
Classe 	Pinopsida
Ordre 	Pinales
Famille 	Cupressaceae

Genre
Thuja
L., 1753

Classification phylogénétique
Classification phylogénétique Ordre 	Pinales
Famille 	Cupressaceae

Espèces de rang inférieur

Thuja koraiensis
Thuja occidentalis
Thuja plicata
Thuja standishii
Thuja sutchuenensis
*/

/* GENVRier
Classification Règne 	Plantae
Sous-règne 	Tracheobionta
Division 	Pinophyta
Classe 	Pinopsida
Ordre 	Pinales
Famille 	Cupressaceae

Genre
Juniperus
*/

////////////////////////////////
/* ****************
* SAVE2           *
*******************/

/* TEST 1 */
DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.classification_complete(IN taxon_id INT, OUT _classification TEXT)
	BEGIN
		SET @_classification = SELECT t.taxon FROM `Eucaryotes`.taxonomie WHERE t.id = taxon_id|
		Call get_id_parent(taxon_id, @current_parent_id)|
		WHILE @parent_id <> -1 DO
			@current_taxon = SELECT t.taxon FROM `Eucaryotes`.taxonomie WHERE t.id = current_parent_id|
			@_classification = CONCAT(@_classification, @current_taxon)|
			Call get_id_parent(taxon_id, @parent_id)|
	END |
DELIMITER ;

/* TEST 2 */
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.classification_complete(IN taxon_id INT, OUT classification TEXT)
	BEGIN
		SELECT CONCAT (
            Call get_id_parent(taxon_id, @current_parent_id)
            WHILE @current_parent_id != 2 DO
            	Call get_id_parent(@current_parent_id, @current_parent_id)
        ) INTO classification;
	END |
DELIMITER ;

CALL `Eucaryotes`.classification_complete(122, @hehe);
SELECT @hehe;

/* FUNCTION 1 */
DELIMITER |
CREATE FUNCTION classification(taxon_id INT) RETURNS VARCHAR(255)
DETERMINISTIC
BEGIN
    DECLARE retorno VARCHAR(255);
    SET retorno = "";
    CALL get_id_parent(taxon_id, @current_parent_id)
    WHILE current_parent_id <> -1 DO
        SET retorno = CONCAT(retorno,SELECT t.taxon FROM taxonomie t WHERE t.id = current_parent_id);
        CALL get_id_parent(taxon_id, @current_parent_id);
    END WHILE;
    RETURN retorno;
END|
DELIMITER ;

classification(122);