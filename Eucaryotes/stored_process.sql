DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.afficher_id_parent_avec_id(IN taxon_id INT)
	BEGIN
		SELECT t.parent_id FROM `Eucaryotes`.`taxonomie` t WHERE t.id = taxon_id;
	END |
DELIMITER ;

-- CALL afficher_id_parent_avec_id(122);
DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.get_id_parent(IN taxon_id INT, OUT parent_id INT)
	BEGIN
		SELECT t.parent_id INTO parent_id FROM `Eucaryotes`.`taxonomie` t WHERE t.id = taxon_id;
	END |
DELIMITER ;

-- CALL get_id_parent(122, @parent);
-- SELECT @parent AS "parent_id";

DELIMITER |
CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.recuperer_nom_parent(IN taxon_id INT)
	BEGIN
		Call get_id_parent(taxon_id, @output);
		SELECT t.taxon as "Parent" from taxonomie t where t.id = @output;
	END |
DELIMITER ;


-- :^)
-- https://dev.mysql.com/doc/refman/8.0/en/with.html

DELIMITER |

CREATE PROCEDURE IF NOT EXISTS `Eucaryotes`.classification(IN taxon_id INT)
BEGIN
    WITH RECURSIVE recursive_concat AS (
        SELECT taxon, parent_id, CAST(taxon AS CHAR(1000)) AS path
        FROM taxonomie
        WHERE id = taxon_id
        UNION ALL
        SELECT t.taxon, t.parent_id, CONCAT(recursive_concat.path, '-', t.taxon)
        FROM taxonomie t
        JOIN recursive_concat ON t.id = recursive_concat.parent_id
    )
    SELECT path FROM recursive_concat;
END |

DELIMITER ;
-- CALL classification(122);
-- todo => return
-- todo => add counter order by limit 1
-- todo => only id and get all classification.information_taxon (122-121-50-16-1- => all diseases)
